#include<stdio.h>
#include<conio.h>
#include<ctype.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>
#include<math.h>

//void main_24();
void game_24(char dif[], int goal);
int checkAnswer_24(char jawab[], char dif[], int num1, int num2, int num3, int num4);
void exitAnim();

int operation_24(char op, int num1, int num2){
	int res;
	switch(op){
		case '+':
			res = num1+num2;
			break;
		case '-':
			res = num1-num2;
			break;
		case 'x':
			res = num1*num2;
			break;
		case ':':
			res = num1/num2;
			break;
	}
	return res;
}


void main_24(){
	char dif_24[5];
	int goal_24;
	char choiceDif_24;
	char choiceScore_24;
	char confirm_24;
	for(int j=0; j<1; j++){
		system("cls");
		printf("___________________________________________________\n");
		printf("                        24                         \n");
		printf("___________________________________________________\n");
		printf("\n");
		printf("\n");
		printf("___________________________________________________\n");
		printf("Pilih Tingkat Kesulitan:\n");
		printf("Easy: Semua empat angka tidak harus dipakai\n");
		printf("Hard: Semua empat angka HARUS dipakai\n");
		printf("\n");
		printf("===> Tekan E untuk memilih Easy\n");
		printf("===> Tekan H untuk memilih Hard\n");
		//pilih difficulty
		for(int i=0; i<1; i++){
			choiceDif_24=toupper(getch());
			switch(choiceDif_24){
				case 'E':
					strcpy(dif_24,"Easy");
					break;
				case 'H':
					strcpy(dif_24,"Hard");
					break;
				default:
					i--;
					break;
			}	
		}
		system("cls");
		printf("___________________________________________________\n");
		printf("                        24                         \n");
		printf("___________________________________________________\n");
		printf("\n");
		printf("\n");
		printf("___________________________________________________\n");
		printf("Tingkat Kesulitan: %s\n", dif_24);
		printf("\n");
		printf("Pilih jawaban benar yang diperlukan untuk Menang:\n");
		printf("\n");
		printf("===> Tekan 1 untuk memilih 1 soal\n");
		printf("===> Tekan 2 untuk memilih 3 soal\n");
		printf("===> Tekan 3 untuk memilih 5 soal\n");
		printf("===> Tekan 4 untuk memilih 10 soal\n");
		for(int i=0; i<1; i++){
			choiceScore_24=toupper(getch());
			switch(choiceScore_24){
				case '1':
					goal_24 = 1;
					break;
				case '2':
					goal_24 = 3;
					break;
				case '3':
					goal_24 = 5;
					break;
				case '4':
					goal_24 = 10;
					break;
				default:
					i--;
					break;
			}
		}
		system("cls");
		printf("___________________________________________________\n");
		printf("                        24                         \n");
		printf("___________________________________________________\n");
		printf("\n");
		printf("\n");
		printf("___________________________________________________\n");
		printf("Tingkat Kesulitan: %s\n", dif_24);
		printf("Jawaban benar yang diperlukan untuk menang: %d\n", goal_24);
		printf("___________________________________________________\n");
		printf("\n");
		printf("\n");
		printf("Apakah pilihan ini sudah benar?\n");
		printf("\n");
		printf("===> Tekan Y bila sudah benar\n");
		printf("===> Tekan N bila ingin mengubah pilihan\n");	
		for(int i=0; i<1; i++){
			confirm_24=toupper(getch());
			switch(confirm_24){
				case 'Y':
					break;
				case 'N':
					break;
				default:
					i--;
					break;
			}
		}
		if(confirm_24 == 'N'){
			j--;
			continue;
		}
		system("cls");
		printf("___________________________________________________\n");
		printf("                        24                         \n");
		printf("___________________________________________________\n");
		printf("\n");
		printf("\n");
		printf("___________________________________________________\n");
		printf("Tip: ketik ""skip"" untuk mengganti soal\n\n");
		switch(choiceDif_24){
			case 'E':
				printf("Easy: Bila sudah melihat sesuatu yang menghasilkan\n");
				printf("24, langsung ketik jawabannya!\n");
				break;
			case 'H':
				printf("Hard: Ingat, keempat angka pada soal HARUS digunakan!\n");
				printf("Bila tidak menemukan jawaban, langsung ketik skip.\n");
				break;
		}
		printf("___________________________________________________\n");
		printf("===> Tekan tombol apa saja untuk memulai permainan\n");
	}
	while(toupper(getch())){
	  game_24(dif_24, goal_24);
	}
}

int checkAnswer_24(char jawab[], char dif[], int num1, int num2, int num3, int num4){
	int res;
	int len=strlen(jawab);
	int angka[4] = {0};
	int flagAngka[4] = {0};
	char operasi[4];
	operasi[0] = '0';
	operasi[1] = '0';
	operasi[2] = '0';
	for(int i=0;i<len;i++){
		int temp = 0;
		//cek angka
		if(jawab[i]>='0'&&jawab[i]<='9'){
			//cek angka 10:
			if(jawab[i]=='1' && jawab[i+1]=='0'){
				temp = 10;
			} else{
				temp = jawab[i]-'0';
			}
			//store angka:
			if(angka[0]==0&&angka[1]==0&&angka[2]==0&&angka[3]==0){
				angka[0] = temp;
			}else{
				if(angka[1]==0&&angka[2]==0&&angka[3]==0){
				angka[1] = temp;
				}else{
					if(angka[2]==0&&angka[3]==0){
					angka[2] = temp;
					}else{
						if(angka[3]==0){
							angka[3] = temp;
						}
					}
				}
			}
		}
		//cek operasi
		if(jawab[i]=='+'||jawab[i]=='-'||jawab[i]=='x'||jawab[i]==':'){
			//store operasi
			if(operasi[0]=='0'&&operasi[1]=='0'&&operasi[2]=='0'){
				operasi[0] = jawab[i];
			} else{
				if(operasi[1]=='0'&&operasi[2]=='0'){
					operasi[1] = jawab[i];
				} else{
					if(operasi[2]=='0'){
						operasi[2] = jawab[i];
					}
				}
			}
		}
	}
	if(strcmp(dif, "Hard") == 0){
		for(int i=0;i<4;i++){
			if(angka[i] == 0){
				printf("Angka yang kamu gunakan tidak ada di soal\n");
				printf("===> Tekan tombol apa saja untuk lanjut\n");
				while(toupper(getch())){
		  			return 0;
				}
			}
		}
	}
	//printf("%d %d %d %d\n", angka[0], angka[1], angka[2], angka[3]);
	//printf("%c %c %c\n", operasi[0], operasi[1], operasi[2]);

	//cek angka yg dipake ada di soal ato nggak
	for(int i=0;i<4;i++){
		if(angka[i] == num1 && flagAngka[i] == 0){
			flagAngka[i]++;
		}
	}
	for(int i=0;i<4;i++){
		if(angka[i] == num2 && flagAngka[i] == 0){
			flagAngka[i]++;
		}
	}
	for(int i=0;i<4;i++){
		if(angka[i] == num3 && flagAngka[i] == 0){
			flagAngka[i]++;
		}
	}
	for(int i=0;i<4;i++){
		if(angka[i] == num4 && flagAngka[i] == 0){
			flagAngka[i]++;
		}
	}
	for(int i=0;i<4;i++){
		if(angka[i] !=0 && flagAngka[i] == 0){
			printf("Angka yang kamu gunakan tidak ada di soal\n");
			printf("===> Tekan tombol apa saja untuk lanjut\n");
			while(toupper(getch())){
	  			return 0;
			}	
		}
	}
	
	//cek apakah angka yang dipake jumlahnya sesuai (gak kelebihan)
	int angkaJawab[11] = {0};
	int angkaSoal[11] = {0};
	
	angkaSoal[num1]++;
	angkaSoal[num2]++;
	angkaSoal[num3]++;
	angkaSoal[num4]++;
	
	for(int i=0;i<4;i++){
		angkaJawab[angka[i]]++;
	}
	
	for(int i=0;i<4;i++){
		if(angka[i] !=0){
			if(angkaJawab[angka[i]] > angkaSoal[angka[i]]){
				printf("Angka yang kamu gunakan tidak ada di soal\n");
				printf("===> Tekan tombol apa saja untuk lanjut\n");
				while(toupper(getch())){
		  			return 0;
				}
			}
		}
	}
	
	//operasi 1:
	int res1 = operation_24(operasi[0], angka[0], angka[1]);
	int res2 = operation_24(operasi[1], res1, angka[2]);
	int res3 = operation_24(operasi[2], res2, angka[3]);
	printf("= %d\n\n", res3);
	//cek jawaban:
	if(res3==24){
		printf("Jawaban BENAR!!!\n");
		printf("===> Tekan tombol apa saja untuk lanjut\n");
		while(toupper(getch())){
	  		return 1;
		}
	}else{
		printf("Jawaban SALAH!!!\n");
		printf("===> Tekan tombol apa saja untuk lanjut\n");
		while(toupper(getch())){
	  		return 0;
		}
	}
}

void game_24(char dif[], int goal){
	time_t startTime;
	time(&startTime);
	system("cls");
	int i=0;
	int skip=0, wa=0;
	for(i=0; i<goal; i++){
		printf("___________________________________________________\n");
		printf("Tingkat kesulitan: %s\n", dif);
		printf("Jawaban benar: %d dari %d\n", i, goal);
		printf("___________________________________________________\n\n");
		srand (time(NULL));
		int num1 = rand() % 10 + 1;
		srand (time(NULL)*time(NULL));
		int num2 = rand() % 10 + 1;
		srand (time(NULL)/num2*num1);
		int num3 = rand() % 10 + 1;
		srand (time(NULL)*num3*100);
		int num4 = rand() % 10 + 1;
		printf("[%d] [%d] [%d] [%d]\n\n", num1, num2, num3, num4);
		char jawab[100];
		scanf("%s", jawab);
		if(strcmp(jawab, "skip") == 0){
			i--;
			skip++;
			system("cls");
			continue;
		}
		if(checkAnswer_24(jawab, dif, num1, num2, num3, num4) == 0){
			i--;
			wa++;
		}
		system("cls");
	}
	
	time_t endTime;
	time(&endTime);
	printf("___________________________________________________\n");
	printf("Tingkat kesulitan: %s\n", dif);
	printf("Jawaban benar: %d dari %d\n", i, goal);
	printf("___________________________________________________\n\n");
	printf("Selamat! Kamu sudah memenangkan permainan!!\n");
	double totalTime = difftime(endTime, startTime);
	printf("Waktu untuk menyesaikan permainan   : %.f detik\n", totalTime);
	printf("Soal yang dilewati                  : %d\n", skip);
	printf("Jawaban yang salah                  : %d\n", wa);
	printf("\n");
	printf("\n");
	printf("===> Tekan M untuk kembali ke menu permainan 24\n");
	printf("===> Tekan Q untuk keluar dari aplikasi\n");
	char postGame_24;
	for(int i=0; i<1; i++){
		postGame_24=toupper(getch());
		switch(postGame_24){
			case 'M':
				main_24();
				break;
			case 'Q':
				exitAnim();
			default:
				i--;
				break;
		}
	}
}

void exitAnim(){
	system("cls");
	printf("                                    )                \n");
	printf("  *   )                          ( /(             )  \n");
	printf("` )  /(  (  (  (     )      )    )\\())  )   (  ( /(  \n");
	printf(" ( )(_))))\\ )( )\\   (    ( /(  |((_)\\( /( ( )\\ )\\()) \n");
	printf("(_(_())/((_|()((_)  )\\  ')(_)) |_ ((_)(_)))((_|(_)\\  \n");
	printf("|_   _(_))  ((_|_)_((_))((_)_  | |/ ((_)_((_|_) |(_) \n");
	printf("  | | / -_)| '_| | '  \\() _` | | ' </ _` (_-< | ' \\  \n");
	printf("  |_| \\___||_| |_|_|_|_|\\__,_| |_|\\_\\__,_/__/_|_||_| \n");
	exit(1);
}
