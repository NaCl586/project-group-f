#include<stdio.h>
#include<conio.h>
#include<ctype.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>

//EASY
char LibraryTypingIII[25][4]= {"dia", "aku", "ada", "dua", "api", "air", "ibu", "pin", "pen", "apa", "ini", "dan", "mau", "itu", "tas", "jus", "uji", "hal", "isi", "kan", "mei", "mal", "tua", "ayo", "adu"};
char LibraryTypingIV[25][5] = {"kamu", "saya", "kami", "juni", "juli", "akan", "kata", "yang", "satu", "tiga", "lima", "enam", "diam", "muda", "juga", "sore", "dari", "lain", "anak", "bayi", "main", "ayah", "nama", "atau", "asal"};
char LibraryTypingV[25][6] = {"makan", "angin", "minum", "dalam", "cinta", "tahun", "karya", "tidak", "lemah", "masih", "sejak", "nenek", "paman", "kakek", "tante", "hitam", "putih", "warna", "jalan", "rumah", "mandi", "malam", "siang", "subuh", "masak"};
char LibraryTypingVI[25][7] = {"pulang", "hutang", "sampu", "sandar", "tampar", "emping", "sambal", "santun", "syukur", "senang", "laptop", "santai", "bacang", "kepala", "anjing", "burung", "sultan", "karate", "basket", "mereka", "amilum", "sekutu", "jingga", "sablon", "fisika"};
//HARD
char LibraryTypingVII[25][8] = {"inggris", "biologi", "belajar", "senyawa", "telepon", "tunggal", "tinggal", "selimut", "gerbong", "listrik", "farmasi", "sendiri", "ekonomi", "sejarah", "lembaga", "webinar", "samsung", "ajaklah", "membaca", "filipus", "peserta", "memberi", "menjadi", "penulis", "membuat"};
char LibraryTypingVIII[25][9] = {"larangan", "kekayaan", "mengurus", "malaikat", "kekayaan", "kegunaan", "semangat", "kehendak", "mengutip", "akademis", "perasaan", "komputer", "sumedang", "magelang", "tayangan", "bhinneka", "televisi", "nganggur", "membunuh", "keluarga", "pahlawan", "kalender", "produksi", "konsumsi", "meminjam"};
char LibraryTypingIX[25][10] = {"indonesia", "nusantara", "sriwijaya", "majapahit", "gajahmada", "pendakian", "borobudur", "prambanan", "demokrasi", "pancasila", "kesalahan", "perbuatan", "eksekutif", "sederhana", "kelewatan", "sederajat", "kegigihan", "dirgahayu", "terkadang", "manajemen", "kebiasaan", "kegagalan", "pekerjaan", "kejujuran", "kekesalan"};
char LibraryTypingX[25][11] = {"yogyakarta", "lukisannya", "perjuangan", "kemiskinan", "menghitung", "penjelasan", "kesenangan", "matematika", "distribusi", "bertanding", "mencampuri", "kalimantan", "menentukan", "singkawang", "menganggap", "purwokerto", "bangladesh", "pengertian", "sebelumnya", "sesudahnya", "berlindung", "perkebunan", "penjajahan", "kolesterol","penggunaan"};

void game_Typing(char dif[], int goal);
void main_Typing();
void Animation();

//int main(){
//	main_Typing();
//	return 0;
//}

void main_Typing(){
	char dif_Typing[5];
	int goal_Typing;
	char choiceDif_Typing;
	char choiceScore_Typing;
	char confirm_Typing;
	for(int j=0; j<1; j++){
		system("cls");
		printf("___________________________________________________\n");
		printf("                    TYPING GAME                    \n");
		printf("___________________________________________________\n");
		printf("\n");
		printf("\n");
		printf("___________________________________________________\n");
		printf("Pilih Tingat Kesulitan:\n");
		printf("Easy: Panjang tiap kata 3 sampai 6 huruf\n");
		printf("Hard: Panjang tiap kata 7 sampai 10 huruf\n");
		printf("\n");
		printf("===> Tekan E untuk memilih Easy\n");
		printf("===> Tekan H untuk memilih Hard\n");
		//pilih difficulty
		for(int i=0; i<1; i++){
			choiceDif_Typing=toupper(getch());
			switch(choiceDif_Typing){
				case 'E':
					strcpy(dif_Typing,"Easy");
					break;
				case 'H':
					strcpy(dif_Typing,"Hard");
					break;
				default:
					i--;
					break;
			}	
		}
		system("cls");
		printf("___________________________________________________\n");
		printf("                    TYPING GAME                    \n");
		printf("___________________________________________________\n");
		printf("\n");
		printf("\n");
		printf("___________________________________________________\n");
		printf("Tingkat Kesulitan: %s\n", dif_Typing);
		printf("\n");
		printf("Pilih jumlah kata yang perlu diketik untuk menang:\n");
		printf("\n");
		printf("===> Tekan 1 untuk memilih 10 kata\n");
		printf("===> Tekan 2 untuk memilih 30 kata\n");
		printf("===> Tekan 3 untuk memilih 50 kata\n");
		printf("===> Tekan 4 untuk memilih 100 kata\n");
		for(int i=0; i<1; i++){
			choiceScore_Typing=toupper(getch());
			switch(choiceScore_Typing){
				case '1':
					goal_Typing = 10;
					break;
				case '2':
					goal_Typing = 30;
					break;
				case '3':
					goal_Typing = 50;
					break;
				case '4':
					goal_Typing = 100;
					break;
				default:
					i--;
					break;
			}
		}
		system("cls");
		printf("___________________________________________________\n");
		printf("                    TYPING GAME                    \n");
		printf("___________________________________________________\n");
		printf("\n");
		printf("\n");
		printf("___________________________________________________\n");
		printf("Tingkat Kesulitan: %s\n", dif_Typing);
		printf("Jumlah kata yang perlu diketik untuk menang: %d\n", goal_Typing);
		printf("___________________________________________________\n");
		printf("\n");
		printf("\n");
		printf("Apakah pilihan ini sudah benar?\n");
		printf("\n");
		printf("===> Tekan Y bila sudah benar\n");
		printf("===> Tekan N bila ingin mengubah pilihan\n");	
		for(int i=0; i<1; i++){
			confirm_Typing=toupper(getch());
			switch(confirm_Typing){
				case 'Y':
					break;
				case 'N':
					break;
				default:
					i--;
					break;
			}
		}
		if(confirm_Typing == 'N'){
			j--;
			continue;
		}
		system("cls");
		printf("___________________________________________________\n");
		printf("                    TYPING GAME                    \n");
		printf("___________________________________________________\n");
		printf("\n");
		printf("\n");
		printf("___________________________________________________\n");
		printf("Tip: Pelan tapi akurat lebih baik daripada cepat\ntapi banyak kesalahan.\n\n");
		switch(choiceDif_Typing){
			case 'E':
				printf("Easy: Kata hanya memiliki 3-6 huruf!\nGunakan reflekmu untuk mengetik dengan cepat!\n");
				break;
			case 'H':
				printf("Hard: Kata bisa memiliki panjang antara 7-10 huruf!\nHati-hati sebelum mengetik!\n");
				break;
		}
		printf("___________________________________________________\n");
		printf("===> Tekan tombol apa saja untuk memulai permainan\n");
	}
	while(toupper(getch())){
	  game_Typing(dif_Typing, goal_Typing);
	}
}

void game_Typing(char dif[], int goal){
	time_t startTime;
	time(&startTime);
	int i = 0;
	int error =0;
	system("cls");
	for(i=0; i<goal; i++){
		printf("___________________________________________________\n");
		printf("Tingkat kesulitan: %s\n", dif);
		printf("Kata yang sudah diketik dengan benar: %d dari %d\n", i, goal);
		printf("___________________________________________________\n\n");
		//randomize panjang kata
		srand (time(NULL));
		int difConstant;
		strcmp(dif, "Hard") == 0 ? difConstant = 4 : difConstant = 0;
		int panjangKata = rand() % 4 + 4 + difConstant;
		
		//randomize kata
		srand (time(NULL)*time(NULL)/panjangKata);
		int rng = rand() % 25;
		char kata[panjangKata];
		switch(panjangKata){
			case 4:
				strcpy(kata, LibraryTypingIII[rng]);
				break;
			case 5:
				strcpy(kata, LibraryTypingIV[rng]);
				break;
			case 6:
				strcpy(kata, LibraryTypingV[rng]);
				break;
			case 7:
				strcpy(kata, LibraryTypingVI[rng]);
				break;
			case 8:
				strcpy(kata, LibraryTypingVII[rng]);
				break;
			case 9:
				strcpy(kata, LibraryTypingVIII[rng]);
				break;
			case 10:
				strcpy(kata, LibraryTypingIX[rng]);
				break;
			case 11:
				strcpy(kata, LibraryTypingX[rng]);
				break;
		}
		char jawab[101];
		printf("%s\n", kata);
		scanf("%s", &jawab);
		getchar();
		for(int j=0;j<panjangKata;j++){
			if(jawab[j]>='A' && jawab[j]<='Z'){
				jawab[j] += 32;
			}
		}
		system("cls");
		if(strcmp(jawab, kata)!=0){
			i--;
			error++;
		}
	}
	time_t endTime;
	time(&endTime);
	system("cls");
	printf("___________________________________________________\n");
	printf("Tingkat kesulitan: %s\n", dif);
	printf("Kata yang sudah diketik dengan benar: %d dari %d\n", i, goal);
	printf("___________________________________________________\n\n");
	printf("Selamat! Kamu sudah memenangkan permainan!!\n");
	double totalTime = difftime(endTime, startTime);
	printf("Waktu untuk menyesaikan permainan   : %.0f detik\n", totalTime);
	printf("Jumlah kata yang typo               : %d kata\n", error);
	double akurasi = (100*goal)/(goal+error);
	printf("Akurasi pengetikan                  : %.0f%%\n\n", akurasi);
	printf("Kecepatan mengetik (kasar)          : %.0f KPM\n", (goal+error)*60/totalTime);
	printf("Kecepatan mengetik kasar menganggap pengetikan\nkata yang typo.\n\n");
	printf("Kecepatan mengetik (bersih)         : %.0f KPM\n", (goal)*60/totalTime);
	printf("Kecepatan mengetik bersih tidak menganggap\npengetikan kata yang typo\n\n");
	printf("\n");
	printf("\n");
	printf("===> Tekan M untuk kembali ke menu permainan typing\n");
	printf("===> Tekan Q untuk keluar dari aplikasi\n");
	char postGame_Typing;
	for(int i=0; i<1; i++){
		postGame_Typing=toupper(getch());
		switch(postGame_Typing){
			case 'M':
				main_Typing();
				break;
			case 'Q':
				Animation();
			default:
				i--;
				break;
		}
	}
	return;
}

void Animation(){
	system("cls");
	printf("                                    )                \n");
	printf("  *   )                          ( /(             )  \n");
	printf("` )  /(  (  (  (     )      )    )\\())  )   (  ( /(  \n");
	printf(" ( )(_))))\\ )( )\\   (    ( /(  |((_)\\( /( ( )\\ )\\()) \n");
	printf("(_(_())/((_|()((_)  )\\  ')(_)) |_ ((_)(_)))((_|(_)\\  \n");
	printf("|_   _(_))  ((_|_)_((_))((_)_  | |/ ((_)_((_|_) |(_) \n");
	printf("  | | / -_)| '_| | '  \\() _` | | ' </ _` (_-< | ' \\  \n");
	printf("  |_| \\___||_| |_|_|_|_|\\__,_| |_|\\_\\__,_/__/_|_||_| \n");
	exit(1);
}
