#include<stdio.h>
#include<conio.h>
#include<ctype.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>
#include<math.h>

void game_Quiz(char dif[], int goal);
int soalHard(int rng);
int soalEasy(int rng);
void Animation_Quiz();

void lower(char s[]){
	for(int i=0;s[i]!='\0';i++){
		if(s[i]>='A' && s[i]<='Z'){
			s[i] += 32;
		}
	}
}

void main_Quiz(){
	char dif_Quiz[5];
	int goal_Quiz;
	char choiceDif_Quiz;
	char choiceScore_Quiz;
	char confirm_Quiz;
	for(int j=0; j<1; j++){
		system("cls");
		printf("___________________________________________________\n");
		printf("                    QUIZ GAME                      \n");
		printf("___________________________________________________\n");
		printf("\n");
		printf("\n");
		printf("___________________________________________________\n");
		printf("Pilih Tingat Kesulitan:\n");
		printf("Easy: Soal berupa pilihan ganda\n");
		printf("Hard: Soal berupa isian satu kata\n");
		printf("\n");
		printf("===> Tekan E untuk memilih Easy\n");
		printf("===> Tekan H untuk memilih Hard\n");
		//pilih difficulty
		for(int i=0; i<1; i++){
			choiceDif_Quiz=toupper(getch());
			switch(choiceDif_Quiz){
				case 'E':
					strcpy(dif_Quiz,"Easy");
					break;
				case 'H':
					strcpy(dif_Quiz,"Hard");
					break;
				default:
					i--;
					break;
			}	
		}
		system("cls");
		printf("___________________________________________________\n");
		printf("                    QUIZ GAME                      \n");
		printf("___________________________________________________\n");
		printf("\n");
		printf("\n");
		printf("___________________________________________________\n");
		printf("Tingkat Kesulitan: %s\n", dif_Quiz);
		printf("\n");
		printf("Pilih jumlah soal yang perlu dijawab benar:\n");
		printf("\n");
		printf("===> Tekan 1 untuk memilih 5 soal\n");
		printf("===> Tekan 2 untuk memilih 10 soal\n");
		printf("===> Tekan 3 untuk memilih 25 soal\n");
		for(int i=0; i<1; i++){
			choiceScore_Quiz=toupper(getch());
			switch(choiceScore_Quiz){
				case '1':
					goal_Quiz = 5;
					break;
				case '2':
					goal_Quiz = 10;
					break;
				case '3':
					goal_Quiz = 25;
					break;
				default:
					i--;
					break;
			}
		}
		system("cls");
		printf("___________________________________________________\n");
		printf("                    QUIZ GAME                      \n");
		printf("___________________________________________________\n");
		printf("\n");
		printf("\n");
		printf("___________________________________________________\n");
		printf("Tingkat Kesulitan: %s\n", dif_Quiz);
		printf("Jumlah soal yang perlu dijawab benar: %d\n", goal_Quiz);
		printf("___________________________________________________\n");
		printf("\n");
		printf("\n");
		printf("Apakah pilihan ini sudah benar?\n");
		printf("\n");
		printf("===> Tekan Y bila sudah benar\n");
		printf("===> Tekan N bila ingin mengubah pilihan\n");	
		for(int i=0; i<1; i++){
			confirm_Quiz=toupper(getch());
			switch(confirm_Quiz){
				case 'Y':
					break;
				case 'N':
					break;
				default:
					i--;
					break;
			}
		}
		if(confirm_Quiz == 'N'){
			j--;
			continue;
		}
		system("cls");
		printf("___________________________________________________\n");
		printf("                    QUIZ GAME                      \n");
		printf("___________________________________________________\n");
		printf("___________________________________________________\n");
		printf("Tip: Gunakan logikamu! Pikirkan sesuatu out of box!\n\n");
		switch(choiceDif_Quiz){
			case 'E':
				printf("Easy: Analisis semua pilihan,pilihlah jawaban\n");
				printf("      yang menurutmu paling benar\n");
				break;
			case 'H':
				printf("Hard: Ingat, jawaban HANYA satu kata! oleh\n");
				printf("      karena itu pikirkan sesuatu out of box\n");
				break;
		}
		printf("___________________________________________________\n");
		printf("___________________________________________________\n");
		printf("===> Tekan tombol apa saja untuk memulai permainan\n");
		printf("___________________________________________________\n");
		printf("___________________________________________________\n");
	}
	while(toupper(getch())){
	  game_Quiz(dif_Quiz, goal_Quiz);
	}
}

int salah(){
	printf("\nJawaban SALAH!!!\n");
	printf("Tekan tombol apa saja untuk lanjut\n");
	while(toupper(getch())){
	  	return 0;
	}
}

int benar(){
	printf("\nJawaban BENAR!!!\n");
	printf("Tekan tombol apa saja untuk lanjut\n");
	while(toupper(getch())){
	  	return 1;
	}
}

int soalHard(int rng){
	char jawabanHard[50];
	switch(rng){
		case 0:
			printf("Lanjutkan pola ini:\nO, T, T, F, F, S, S, ...\n");
			printf("\n");
			scanf("%s", &jawabanHard);
			getchar();
			lower(jawabanHard);
			if(strcmp(jawabanHard, "e") == 0){
				return benar();
			} else{
				return salah();
			}
		case 1:    
			printf("Seorang petani mengkombinasikan dua tumpukan semen \n");
			printf("dengan tiga yang lain. Berapa banyak tumpukan semen \n");
			printf("yang ia miliki sekarang?\n");
			printf("\n");
			scanf("%s", &jawabanHard);
			getchar();
			lower(jawabanHard);
			if(strcmp(jawabanHard, "1") == 0 ){
				return benar();
			} else{
				return salah();
			}
		case 2:
			printf("Hidung : mengendus = Lidah : ...\n");
			printf("\n");
			scanf("%s", &jawabanHard);
			getchar();
			lower(jawabanHard);
			if(strcmp(jawabanHard, "mengecap") == 0){
				return benar();
			} else{
				return salah();
			}
		case 3:
			printf("Haus : Minum = Lapar : ...\n");
			printf("\n");
			scanf("%s", &jawabanHard);
			getchar();
			lower(jawabanHard);
			if(strcmp(jawabanHard, "makan") == 0){
				return benar();
			} else{
				return salah();
			}
		case 4:
			printf("Isilah titik-titik berikut:\n");
			printf("16 06 68 88 .. 98\n");
			printf("\n");
			scanf("%s", &jawabanHard);
			getchar();
			lower(jawabanHard);
			if(strcmp(jawabanHard, "78") == 0){
				return benar();
			} else{
				return salah();
			}
		case 5:
			printf("Dibeli berwarna hitam, dipakai warnanya merah, \n");
			printf("dibuang warnanya abu-abu, apakah itu?\n");
			printf("\n");
			scanf("%s", &jawabanHard);
			getchar();
			lower(jawabanHard);
			if(strcmp(jawabanHard, "arang") == 0){
				return benar();
			} else{
				return salah();
			}
		case 6:
			printf("Ditusuk tapi bukan sate, dijaring tapi bukan ikan?\n");
			printf("\n");
			scanf("%s", &jawabanHard);
			getchar();
			lower(jawabanHard);
			if(strcmp(jawabanHard, "konde") == 0){
				return benar();
			} else{
				return salah();
			}
		case 7:
			printf("Pagi-pagi ada 2, siang ada 1, malam gak ada.\nAda di ujung api dan ditengah air, apakah itu?\n");
			printf("\n");
			scanf("%s", &jawabanHard);
			getchar();
			lower(jawabanHard);
			if(strcmp(jawabanHard, "i") == 0){
				return benar();
			} else{
				return salah();
			}
		case 8:
			printf("Apa yang ada di ujung langit?\n");
			printf("\n");
			scanf("%s", &jawabanHard);
			getchar();
			lower(jawabanHard);
			if(strcmp(jawabanHard, "t") == 0){
				return benar();
			} else{
				return salah();
			}
		case 9:
			printf("Lemari apa yang bisa masuk kantong?\n");
			printf("\n");
			scanf("%s", &jawabanHard);
			getchar();
			lower(jawabanHard);
			if(strcmp(jawabanHard, "lemaribu") == 0){
				return benar();
			} else{
				return salah();
			}
		case 10:
			printf("Minuman, minuman apa banyak isinya?\n");
			printf("\n");
			scanf("%s", &jawabanHard);
			getchar();
			lower(jawabanHard);
			if(strcmp(jawabanHard, "float") == 0){
				return benar();
			} else{
				return salah();
			}
		case 11:
			printf("Buah apa yang punya duit banyak\n");
			printf("\n");
			scanf("%s", &jawabanHard);
			getchar();
			lower(jawabanHard);
			if(strcmp(jawabanHard, "srikaya") == 0){
				return benar();
			} else{
				return salah();
			}
		case 12:
			printf("Pesawat jatuh, kapal tenggelam, munculnya di mana?\n");
			printf("\n");
			scanf("%s", &jawabanHard);
			getchar();
			lower(jawabanHard);
			if(strcmp(jawabanHard, "dikoran") == 0 || strcmp(jawabanHard, "koran") == 0){
				return benar();
			} else{
				return salah();
			}
		case 13:
			printf("Ditutup jadi tongkat, dibuka jadi tenda. Apakah itu?\n");
			printf("\n");
			scanf("%s", &jawabanHard);
			getchar();
			lower(jawabanHard);
			if(strcmp(jawabanHard, "payung") == 0){
				return benar();
			} else{
				return salah();
			}
		case 14:
			printf("Binatang apa yang jago renang?\n");
			printf("\n");
			scanf("%s", &jawabanHard);
			getchar();
			lower(jawabanHard);
			if(strcmp(jawabanHard, "bebek") == 0){
				return benar();
			} else{
				return salah();
			}
		case 15:
			printf("Binatang apa yang jago menyelam?\n");
			printf("\n");
			scanf("%s", &jawabanHard);
			getchar();
			lower(jawabanHard);
			if(strcmp(jawabanHard, "ikan") == 0){
				return benar();
			} else{
				return salah();
			}	
		case 16:
			printf("Apa bahasa mandarin dari lantai basah?\n");
			printf("\n");
			scanf("%s", &jawabanHard);
			getchar();
			lower(jawabanHard);
			if(strcmp(jawabanHard, "lhichin") == 0){
				return benar();
			} else{
				return salah();
			}
		case 17:
			printf("Jenis kutu yang amat mengerikan?\n");
			printf("\n");
			scanf("%s", &jawabanHard);
			getchar();
			lower(jawabanHard);
			if(strcmp(jawabanHard, "kutukan") == 0){
				return benar();
			} else{
				return salah();
			}
		case 18:
			printf("Ada 2 benda, benda A 2x lebih panas dari benda B.\nBila suhu benda B itu 27 derajat celsius, berapakah suhu benda A?\n");
			printf("\n");
			scanf("%s", &jawabanHard);
			getchar();
			if(strcmp(jawabanHard, "327") == 0 || strcmp(jawabanHard, "327oc") == 0){
				return benar();
			} else{
				return salah();
			}
		case 19:
			printf("Tahu apa yang berukuran besar?\n");
			printf("\n");
			scanf("%s", &jawabanHard);
			getchar();
			lower(jawabanHard);
			if(strcmp(jawabanHard, "sumedang") == 0){
				return benar();
			} else{
				return salah();
			}
		case 20:
			printf("Benda apa yang keluar ketika diputar?\n");
			printf("\n");
			scanf("%s", &jawabanHard);
			getchar();
			lower(jawabanHard);
			if(strcmp(jawabanHard, "lipstik") == 0 || strcmp(jawabanHard, "lipstick") == 0){
				return benar();
			} else{
				return salah();
			}
		case 21:
			printf("Kayu, kayu apa yang bisa dimakan?\n");
			printf("\n");
			scanf("%s", &jawabanHard);
			getchar();
			lower(jawabanHard);
			if(strcmp(jawabanHard, "kayupuk") == 0){
				return benar();
			} else{
				return salah();
			}
		case 22:
			printf("Lebih sering digunakan banyak orang tapi milik pribadi. Apakah itu?\n");
			printf("\n");
			scanf("%s", &jawabanHard);
			getchar();
			lower(jawabanHard);
			if(strcmp(jawabanHard, "nama") == 0){
				return benar();
			} else{
				return salah();
			}
		case 23:
			printf("Kopi apa yang bisa mencapit?\n");
			printf("\n");
			scanf("%s", &jawabanHard);
			getchar();
			lower(jawabanHard);
			if(strcmp(jawabanHard, "kopiting") == 0){
				return benar();
			} else{
				return salah();
			}
		case 24:
			printf("Suku apa yang banyak berkeliaran di mal?\n");
			printf("\n");
			scanf("%s", &jawabanHard);
			getchar();
			lower(jawabanHard);
			if(strcmp(jawabanHard, "sukuriti") == 0){
				return benar();
			} else{
				return salah();
			}
	}
}

int soalEasy(int rng){
	char jawabanEasy[50];
	switch(rng){
		case 0:
		  //printf("___________________________________________________\n");
			printf("Berapa kali angka 7 muncul di antara bilangan 1 sampai 100?\n");
			printf("A. 10 kali\n");
			printf("B. 15 kali\n");
			printf("C. 18 kali\n");
			printf("D. 20 kali\n");
			printf("\n");
			scanf("%s", &jawabanEasy);
			getchar();
			lower(jawabanEasy);
			if(jawabanEasy[0] == 'd'){
				return benar();
			} else{
				return salah();
			}
		case 1:
		  //printf("___________________________________________________\n");
			printf("Semakin banyak kamu mengambil, semakin banyak yang\ntersisa, apakah itu?\n");
			printf("A. Makanan\n");
			printf("B. Minuman\n");
			printf("C. Sidik Jari\n");
			printf("D. Uang\n");
			printf("\n");
			scanf("%s", &jawabanEasy);
			getchar();
			lower(jawabanEasy);
			if(jawabanEasy[0] == 'c'){
				return benar();
			} else{
				return salah();
			}
		case 2:
		  //printf("___________________________________________________\n");
			printf("Coklat yang dibungkus dalam kemasan menarik sangat laris terjual.\n");
			printf("Coklat Jago dibungkus dalam kemasan berwarna merah menyala.\n");
			printf("Menurut anak-anak, warna merah menyala sangatlah menarik.\n\n");
			printf("Berikut pernyataan manakah yang benar?\n");
			printf("A. Coklat Jago kurang laris terjual di kalangan anak-anak\n");
			printf("B. Coklat Jago tidak laku terjual di kalangan orang dewasa\n");
			printf("C. Coklat Jago laris terjual\n");
			printf("D. Coklat Jago laris terjual di kalangan anak-anak\n");
			printf("\n");
			scanf("%s", &jawabanEasy);
			getchar();
			lower(jawabanEasy);
			if(jawabanEasy[0] == 'd'){
				return benar();
			} else{
				return salah();
			}
		case 3:
		  //printf("___________________________________________________\n");
			printf("Inventor A adalah seorang yang jenius.\n"); 
			printf("Inventor A seorang penemu.\n"); 
			printf("Semua penemu adalah kreatif.\n"); 
			printf("Inventor B juga seorang penemu.\n\n"); 
			printf("Berikut pernyataan manakah yang benar?\n");
			printf("A. Inventor B seorang yang jenius\n");
			printf("B. Inventor B belum tentu kreatif\n");
			printf("C. Inventor A dan Inventor B sama-sama jenius dan kreatif\n");
			printf("D. Inventor B pasti kreatif. Dan belum tentu jenius\n");
			printf("\n");
			scanf("%s", &jawabanEasy);
			getchar();
			lower(jawabanEasy);
			if(jawabanEasy[0] == 'd'){
				return benar();
			} else{
				return salah();
			}
		case 4:
		  //printf("___________________________________________________\n");
			printf("Semua mahasiswa Perguruan Tinggi memiliki Nomor Induk Mahasiswa.\n"); 
			printf("Budi seorang mahasiswa.\n\n");
			printf("Berikut pernyataan manakah yang benar?\n");
			printf("A. Budi mungkin memiliki nomor induk mahasiswa\n");
			printf("B. Belum tentu Budi memiliki nomor induk mahasiswa\n");
			printf("C. Budi memiliki nomor induk mahasiswa\n");
			printf("D. Budi tidak memiliki nomor induk mahasiswa\n");
			printf("\n");
			scanf("%s", &jawabanEasy);
			getchar();
			lower(jawabanEasy);
			if(jawabanEasy[0] == 'c'){
				return benar();
			} else{
				return salah();
			}
		case 5:
		  //printf("___________________________________________________\n");
			printf("Semua tebing berwarna hitam,\n"); 
			printf("sebagian jurang adalah tebing.\n");
			printf("\n");
			printf("Berikut pernyataan manakah yang benar?\n");
			printf("A. Semua jurang berwarna hitam\n");
			printf("B. Sebagian jurang berwarna hitam\n");
			printf("C. Semua yang hitam adalah jurang\n");
			printf("D. Semua yang hitam adalah tebing\n");
			printf("\n");
			scanf("%s", &jawabanEasy);
			getchar();
			lower(jawabanEasy);
			if(jawabanEasy[0] == 'b'){
				return benar();
			} else{
				return salah();
			}
		case 6:
		  //printf("___________________________________________________\n");
			printf("Semua siswa yang lulus ujian pasti pintar.\n"); 
			printf("Sebagian siswa kelas 12 bukan siswa yang pintar.\n");
			printf("\n");
			printf("Berikut pernyataan manakah yang benar?\n");
			printf("A. Sebagian siswa kelas 12 tidak lulus ujian\n");
			printf("B. Semua yang lulus ujian adalah siswa kelas 12\n");
			printf("C. Sebagian siswa yang pandai tidak lulus ujian\n");
			printf("D. Sebagian siswa kelas 12 tidak mengikuti ujian\n");
			printf("\n");
			scanf("%s", &jawabanEasy);
			getchar();
			lower(jawabanEasy);
			if(jawabanEasy[0] == 'a'){
				return benar();
			} else{
				return salah();
			}
		case 7:
		  //printf("___________________________________________________\n");
			printf("Semua tanaman adalah kayu.\n");
			printf("Beberapa jenis kayu adalah keras.\n");
			printf("\n");
			printf("Berikut pernyataan manakah yang benar?\n");
			printf("A. Sebagian kayu yang keras bukan tanaman\n");
			printf("B. Sebagian tanaman bukan kayu\n");
			printf("C. Semua kayu adalah tanaman\n");
			printf("D. Semua kayu yang keras adalah tanaman.\n");
			printf("\n");
			scanf("%s", &jawabanEasy);
			getchar();
			lower(jawabanEasy);
			if(jawabanEasy[0] == 'a'){
				return benar();
			} else{
				return salah();
			}
		case 8:
		  //printf("___________________________________________________\n");
			printf("Semua hutan ditanami pohon.\n");
			printf("Sebagian pohon adalah durian.\n");
			printf("\n");
			printf("Berikut pernyataan manakah yang benar?\n");
			printf("A. Semua ditanami pohon durian\n");
			printf("B. Sebagian ditanami pohon bukan durian\n");
			printf("C. Semua ditanami pohon bukan durian\n");
			printf("D. Sebagian ditanami pohon durian,\n");
			printf("   sebagian tidak ditanami pohon\n");
			printf("\n");
			scanf("%s", &jawabanEasy);
			getchar();
			lower(jawabanEasy);
			if(jawabanEasy[0] == 'b'){
				return benar();
			} else{
				return salah();
			}
		case 9:
		  //printf("___________________________________________________\n");
			printf("Seluruh sawah ditanami padi.\n"); 
			printf("Sebagian padi adalah bibit unggul.\n"); 
			printf("\n");
			printf("Berikut pernyataan manakah yang benar?\n");
			printf("A. Seluruh sawah ditanami padi bukan bibit unggul\n");
			printf("B. Seluruh ditanami padi bibit unggul\n");
			printf("C. Sebagian sawah ditanami padi bibit unggul\n");
			printf("D. Semua tidak ditanami padi bibit unggul\n");
			printf("\n");
			scanf("%s", &jawabanEasy);
			getchar();
			lower(jawabanEasy);
			if(jawabanEasy[0] == 'c'){
				return benar();
			} else{
				return salah();
			}
		case 10:
		  //printf("___________________________________________________\n");
			printf("Apakah 29 - 1 = ...\n");
			printf("A. 28\n");
			printf("B. 29\n");
			printf("C. 30\n");
			printf("D. 31\n");
			printf("\n");
			scanf("%s", &jawabanEasy);
			getchar();
			lower(jawabanEasy);
			if(jawabanEasy[0] == 'c'){
				return benar();
			} else{
				return salah();
			}
		case 11:
		  //printf("___________________________________________________\n");
			printf("Jika B adalah bakso, maka lanjutkan pola ini:\n"); 
			printf("ATB, MMS, AMB, ATB, CLK, ...\n"); 
			printf("A. STTL\n");
			printf("B. SMS\n");
			printf("C. STM\n");
			printf("D. YBB\n");
			printf("\n");
			scanf("%s", &jawabanEasy);
			getchar();
			lower(jawabanEasy);
			if(jawabanEasy[0] == 'a'){
				return benar();
			} else{
				return salah();
			}
		case 12:
		  //printf("___________________________________________________\n");
			printf("Bila 8809 = 6, maka 2581 = ...\n");
			printf("A. 0\n");
			printf("B. 1\n");
			printf("C. 2\n");
			printf("D. 3\n");
			printf("\n");
			scanf("%s", &jawabanEasy);
			getchar();
			lower(jawabanEasy);
			if(jawabanEasy[0] == 'c'){
				return benar();
			} else{
				return salah();
			}
		case 13:
		  //printf("___________________________________________________\n");
			printf("Jika B adalah balon, maka lanjutkan pola ini:\n"); 
			printf("BAL, ... , MKK, HMB, MBH\n");
			printf("A. HLK\n");
			printf("B. KMK\n");
			printf("C. ATW\n");
			printf("D. RRW\n");
			printf("\n");
			scanf("%s", &jawabanEasy);
			getchar();
			lower(jawabanEasy);
			if(jawabanEasy[0] == 'd'){
				return benar();
			} else{
				return salah();
			}
		case 14:
		  //printf("___________________________________________________\n");
			printf("Apakah huruf keempat dalam alfabet?\n");
			printf("A. d\n"); 
			printf("B. c\n");
			printf("C. b\n");
			printf("D. a\n");
			printf("\n");
			scanf("%s", &jawabanEasy);
			getchar();
			lower(jawabanEasy);
			if(jawabanEasy[0] == 'd'){
				return benar();
			} else{
				return salah();
			}
		case 15:
		  //printf("___________________________________________________\n");
			printf("Di mana ada sungai tetapi tidak ada air, kota tetapi\n");
			printf("tidak ada bangunan, dan hutan tanpa pohon?\n");
			printf("A. Peta Kalimantan\n");
			printf("B. Peta Jawa\n");
			printf("C. Peta Indonesia\n");
			printf("D. Peta Dunia\n");
			printf("\n");
			scanf("%s", &jawabanEasy);
			getchar();
			lower(jawabanEasy);
			if(jawabanEasy[0] == 'd'){
				return benar();
			} else{
				return salah();
			}
		case 16:
		  //printf("___________________________________________________\n");
			printf("kilabret aynnabawaj\n");
			printf("A. KO\n");
			printf("B. ?\n");
			printf("C. ???\n");
			printf("D. Gajah\n");
			printf("\n");
			scanf("%s", &jawabanEasy);
			getchar();
			lower(jawabanEasy);
			if(jawabanEasy[0] == 'a'){
				return benar();
			} else{
				return salah();
			}
		case 17:
		  //printf("___________________________________________________\n");
			printf("Dua kereta, Kereta A dan Kereta B, berangkat secara bersamaan dari\n"); 
			printf("Stasiun A dan Stasiun B. Stasiun A dan Stasiun B berjarak 252,5 mil\n"); 
			printf("satu sama lain. Kereta A melaju dengan kecepatan 124,7 mph menuju\n");
			printf("Stasiun B, dan Kereta B bergerak dengan kecepatan 253,5 mph menuju\n"); 
			printf("Stasiun A. Jika kedua kereta berangkat pukul 10:00 dan sekarang pukul\n"); 
			printf("10:08, berapa lama lagi hingga kedua kereta tersebut saling berpapasan?\n");
			printf("A. 31054 menit\n");
			printf("B. 32049 menit\n");
			printf("C. 16232 menit\n");
			printf("D. 32058 menit\n");
			printf("\n");
			scanf("%s", &jawabanEasy);
			getchar();
			lower(jawabanEasy);
			if(jawabanEasy[0] == 'd'){
				return benar();
			} else{
				return salah();
			}
		case 18:
		  //printf("___________________________________________________\n");
			printf("Hari ini hari rabu, 2010 hari kemudian adalah hari?\n");
			printf("A. Kamis\n");
			printf("B. Rabu\n");
			printf("C. Selasa\n");
			printf("D. Senin\n");
			printf("\n");
			scanf("%s", &jawabanEasy);
			getchar();
			lower(jawabanEasy);
			if(jawabanEasy[0] == 'a'){
				return benar();
			} else{
				return salah();
			}
		case 19:
		  //printf("___________________________________________________\n");
			printf("Logam yang paling mudah dioksidasi adalah ...\n");  
			printf("A. Fe\n");
			printf("B. Ag\n");
			printf("C. Pb\n");
			printf("D. Na\n");
			printf("\n");
			scanf("%s", &jawabanEasy);
			getchar();
			lower(jawabanEasy);
			if(jawabanEasy[0] == 'd'){
				return benar();
			} else{
				return salah();
			}
		case 20:
		  //printf("___________________________________________________\n");
			printf("Berapakah hasil dari 10+10x0+10?\n");
			printf("A.0\n");
			printf("B.10\n");
			printf("C.20\n");
			printf("D.200\n");
			printf("\n");
			scanf("%s", &jawabanEasy);
			getchar();
			lower(jawabanEasy);
			if(jawabanEasy[0] == 'c'){
				return benar();
			} else{
				return salah();
			}
		case 21:
		  //printf("___________________________________________________\n");
			printf("Anaknya Siti adalah ibunya anak perempuanku.\n"); 
			printf("Aku siapanya Siti?\n");
			printf("A. Neneknya\n");
			printf("B. Ibunya\n");
			printf("C. Anaknya\n");
			printf("D. Cucunya\n");
			printf("\n");
			scanf("%s", &jawabanEasy);
			getchar();
			lower(jawabanEasy);
			if(jawabanEasy[0] == 'c'){
				return benar();
			} else{
				return salah();
			}
		case 22:
		  //printf("___________________________________________________\n");
			printf("Hasil 1+1+1+1+1+1+1+1+1+1x0+1 adalah?\n");
			printf("A. 10\n");
			printf("B. 1\n");
			printf("C. 0\n");
			printf("D. 11\n");
			printf("\n");
			scanf("%s", &jawabanEasy);
			getchar();
			lower(jawabanEasy);
			if(jawabanEasy[0] == 'a'){
				return benar();
			} else{
				return salah();
			}
		case 23:
		  //printf("___________________________________________________\n");
			printf("Berapa banyak lubang dalam sebuah polo?\n");
			printf("A. 1\n");
			printf("B. 2\n");
			printf("C. 3\n");
			printf("D. 4\n");
			printf("\n");
			scanf("%s", &jawabanEasy);
			getchar();
			lower(jawabanEasy);
			if(jawabanEasy[0] == 'd'){
				return benar();
			} else{
				return salah();
			}
		case 24:
		  //printf("___________________________________________________\n");
			printf("Seekor katak terjatuh kedalam sumur dengan kedalam 30 meter.\n"); 
			printf("Jika di siang hari, katak tersebut berhasil memanjat 5 meter,\n"); 
			printf("namun pada malam hari katak tersebut merosot turun 4 meter.\n"); 
			printf("Begitu terus setiap harinya. Pada hari keberapa katak tersebut\n"); 
			printf("dapat keluar dari sumur?\n");
			printf("A. 25 hari\n");
			printf("B. 30 hari\n");
			printf("C. 26 hari\n");
			printf("D. 31 hari\n");
			printf("\n");
			scanf("%s", &jawabanEasy);
			getchar();
			lower(jawabanEasy);
			if(jawabanEasy[0] == 'c'){
				return benar();
			} else{
				return salah();
			}						
	}
}

void game_Quiz(char dif[], int goal){
	time_t startTime;
	time(&startTime);
	system("cls");
	int i=0;
	int skip=0, wa=0;
	int flagsoal[25] = {0};
	for(i=0; i<goal; i++){
		system("cls");
		printf("___________________________________________________\n");
		printf("Tingkat kesulitan: %s\n", dif);
		printf("Soal %d dari %d\n", i+1, goal);
		printf("___________________________________________________\n\n");
		srand (time(NULL));
		int rng = rand() % 25;
		while(flagsoal[rng] == 1){
			rng = rand() % 25;
		}
		flagsoal[rng] = 1;
		int jawab;
		strcmp(dif, "Hard") == 0 ? jawab = soalHard(rng) : jawab = soalEasy(rng);
		if(jawab == 0){
			break;
		}
		if(jawab == 1){
			continue;
		}
	}
	system("cls");
	time_t endTime;
	time(&endTime);
	printf("___________________________________________________\n");
	i == goal ? printf("                     Selamat!!!!                    \nKamu berhasil menjawab semua pertanyaan dengan benar!!\n\n") : printf("               Permainan berakhir!!                \n          Semoga beruntung lain kali!!\n\n");
	printf("___________________________________________________\n");
	double totalTime = difftime(endTime, startTime);
	printf("Tingkat kesulitan                      : %s\n", dif);
	printf("Lama bermain                           : %.f detik\n", totalTime);
	printf("Pertanyaan yang berhasil dijawab benar : %d/%d\n", i, goal);
	printf("\n");
	printf("\n");
	printf("===> Tekan M untuk kembali ke menu permainan quiz\n");
	printf("===> Tekan Q untuk keluar dari aplikasi\n");
	char postGame_Quiz;
	for(int i=0; i<1; i++){
		postGame_Quiz=toupper(getch());
		switch(postGame_Quiz){
			case 'M':
				main_Quiz();
				break;
			case 'Q':
				Animation_Quiz();
			default:
				i--;
				break;
		}
	}
}

void Animation_Quiz(){
	system("cls");
	printf("                                    )                \n");
	printf("  *   )                          ( /(             )  \n");
	printf("` )  /(  (  (  (     )      )    )\\())  )   (  ( /(  \n");
	printf(" ( )(_))))\\ )( )\\   (    ( /(  |((_)\\( /( ( )\\ )\\()) \n");
	printf("(_(_())/((_|()((_)  )\\  ')(_)) |_ ((_)(_)))((_|(_)\\  \n");
	printf("|_   _(_))  ((_|_)_((_))((_)_  | |/ ((_)_((_|_) |(_) \n");
	printf("  | | / -_)| '_| | '  \\() _` | | ' </ _` (_-< | ' \\  \n");
	printf("  |_| \\___||_| |_|_|_|_|\\__,_| |_|\\_\\__,_/__/_|_||_| \n");
	exit(1);
}
