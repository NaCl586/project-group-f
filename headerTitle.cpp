#include<stdio.h>
#include<string.h>
#include<conio.h>
#include<stdlib.h>
#include<ctype.h>
#include"24.cpp"
#include"typing.cpp"
#include"quiz.cpp"

void mainMenu();
void helpChoice();
void quizGuide();
void typingGuide();
void duaEmpatGuide();
void choiceGame();
void main_24();
void firstPage();
void exitAnimation();
char s='\\';
int main(){
	firstPage();
	return 0;
}

void firstPage(){
	system("cls");
	printf("   __          __  _                            _______\n");
	printf("   %c %c        / / | |                          |__   __|                \n", s,s);
	printf("    %c %c  /%c  / /__| | ___ ___  _ __ ___   ___     | | ___               \n",s,s,s);
	printf("     %c %c/  %c/ / _ %c |/ __/ _ %c| '_ ` _ %c / _ %c    | |/ _ %c              \n",s,s,s,s,s,s,s,s);
	printf("      %c  /%c  /  __/ | (_| (_) | | | | | |  __/    | | (_) |             \n",s,s);
	printf(" __ _ _%c/  %c/ %c___|_|%c___%c___/|_| |_| |_|%c___|    |_|%c___/              \n",s,s,s,s,s,s,s);
	printf("|  ____|         | |          | | (_)                                   \n");
	printf("| |__ _   _ _ __ | |_ __ _ ___| |_ _  ___    __ _  __ _ _ __ ___   ___  \n");
	printf("|  __| | | | '_ %c| __/ _` / __| __| |/ __|  / _` |/ _` | '_ ` _ %c / _ %c \n",s,s,s);
	printf("| |  | |_| | | | | || (_| %c__ %c |_| | (__  | (_| | (_| | | | | | |  __/ \n",s,s);
	printf("|_|   \\__,_|_| |_|%c__%c__,_|___/%c__|_|%c___|  %c__, |%c__,_|_| |_| |_|%c___| \n",s,s,s,s,s,s,s,s,s);
	printf("                                             __/ |                      \n");
	printf("                                            |___/                       \n");
	printf("________________________________________________________________________\n");
	printf("________________________________________________________________________\n");
	printf("===> Tekan tombol apa saja untuk mulai \n");
	printf("________________________________________________________________________\n");
	printf("________________________________________________________________________\n");
	if(toupper(getch())){
		mainMenu();
	}
}


void mainMenu(){
	system("cls");
	printf("___________________________________________________\n");
	printf("                    WELCOME TO                     \n");
	printf("                  FUNTASTIC GAME                   \n");
	printf("___________________________________________________\n");
	printf("___________________________________________________\n");
	printf("===> Tekan S untuk memilih permainan\n");
	printf("===> Tekan H untuk melihat panduan permainan\n");
	printf("===> Tekan Q untuk keluar dari permainan\n");
	printf("___________________________________________________\n");
	printf("___________________________________________________\n");
	char choice;
	for(int i=0; i<1; i++){
		choice=toupper(getch());
		switch(choice){
			case 'Q':
				exitAnimation();
				break;
			case 'H':
				helpChoice();
				break;
			case 'S':
				choiceGame();
				break;
			default:
				i--;
				break;		
		}
	}
}

void helpChoice(){
	system("cls");
	printf("___________________________________________________\n");
	printf("                ~~~BUKU PANDUAN~~~                 \n");
	printf("___________________________________________________\n");
	printf("___________________________________________________\n");
	printf("      Game ini akan dibagi menjadi 3 bagian\n");
	printf("1. Quiz Game\n");
	printf("2. Typing Game\n");
	printf("3. 24 Game\n");
	printf("___________________________________________________\n");
	printf("___________________________________________________\n");
	printf("===> Tekan 1 untuk melihat panduan Quiz Game\n");
	printf("===> Tekan 2 untuk melihat panduan Typing Game\n");
	printf("===> Tekan 3 untuk melihat panduan 24 Game\n");
	printf("===> Tekan Q untuk kembali ke menu utama\n");
	printf("___________________________________________________\n");
	printf("___________________________________________________\n");
	char choice;
	for(int i=0; i<1; i++){
		choice=toupper(getch());
		switch(choice){
			case 'Q':
				mainMenu();
				break;
			case '1':
				quizGuide();
				break;
			case '2':
				typingGuide();
				break;
			case '3':
				duaEmpatGuide();
				break;
			default:
				i--;
				break;		
		}
	}
}

void quizGuide(){
	system("cls");
	printf("___________________________________________________\n");
	printf("                   BUKU PANDUAN                    \n");
	printf("                    QUIZ GAME                      \n");
	printf("___________________________________________________\n");
	printf("___________________________________________________\n");
	printf("Didalam game ini player akan diberikan quiz tentang\n");
	printf("logika. Baik itu logika sederhana ataupun matematika\n");
	printf("\n");  
	printf("Bentuk soal berupa pilihan ganda atau isian.	   \n");
	printf("Soal pilihan ganda dijawab dengan mengetik huruf   \n");
	printf("dari pilihan jawaban yang ingin dijawab dan soal   \n");
	printf("isian dijawab dengan mengetik langsung jawabannya. \n");
	printf("\n"); 
	printf("Player harus menjawab semua pertanyaan yang        \n");
	printf("diberikan dengan benar secara berurutan untuk dapat\n");
	printf("menang. Bila player menjawab salah maka permainan  \n");
	printf("akan berakhir.                                     \n");
	printf("\n");    
	printf("___________________________________________________\n");
	printf("===> Tekan tombol apa saja untuk kembali \n");
	printf("___________________________________________________\n");
	if(toupper(getch())){
		helpChoice();
	}
}

void typingGuide(){
	system("cls");
	printf("___________________________________________________\n");
	printf("                   BUKU PANDUAN                    \n");
	printf("                    TYPING GAME                    \n");
	printf("___________________________________________________\n");
	printf("___________________________________________________\n");
	printf("Didalam game ini player ditugaskan untuk mengetik  \n");
	printf("setiap kata-kata yang muncul. Adapun cara bermain  \n");
	printf("adalah player akan diberikan kata acak. Dimana kata\n");
	printf("tersebut terdiri dari 3-10 huruf.\n");
	printf("\n");
	printf("Tugas dari player adalah mengetik kata yang muncul.\n");
	printf("Apabila sudah selesai mengetik. Silahkan menekan   \n");
	printf("tombol enter untuk mengirim jawaban yang sudah     \n");
	printf("diketikkan. 									   \n");
	printf("\n");
	printf("___________________________________________________\n");
	printf("===> Tekan tombol apa saja untuk kembali \n");
	printf("___________________________________________________\n");
	if(toupper(getch())){
		helpChoice();
	}
}

void duaEmpatGuide(){
	system("cls");
	printf("___________________________________________________\n");
	printf("                   BUKU PANDUAN                    \n");
	printf("                        24                         \n");
	printf("___________________________________________________\n");
	printf("___________________________________________________\n");
	printf("Didalam game ini player akan diberikan 4 angka     \n");
	printf("random. Tugas player adalah mengoperasikan angka   \n");
	printf("tersebut agar dapat menghasilkan angka 24.\n");
	printf("\n");
	printf("Dalam hal ini player boleh menggunakan operasi     \n");
	printf("pertambahan, pengurangan, perkalian dan pembagian. \n");
	printf("Beberapa simbol yang digunakan adalah sebagai      \n");
	printf("berikut (+) sebagai simbol penjumlahan; (-) sebagai\n");
	printf("simbol pengurangan; (x) huruf kecil x sebagai	   \n");
	printf("simbol perkalian; (:) sebagai simbol pembagian	   \n");
	printf("\n");
	printf("Ingat bahwa operasi akan dilakukan dari kiri ke    \n"); 
	printf("kanan secara berurutan tanpa memperhatikan urutan  \n");
	printf("pada aturan aritmatika. Sehingga, pikirkan strategi\n");
	printf("yang tepat untuk menyelesaikan semua soal.         \n");
	printf("___________________________________________________\n");
	printf("===> Tekan tombol apa saja untuk kembali \n");
	printf("___________________________________________________\n");
	if(toupper(getch())){
		helpChoice();
	}
}

void choiceGame(){
	system("cls");
	printf("___________________________________________________\n");
	printf("               SILAHKAN MEMILIH PERMAINAN          \n");
	printf("                   YANG AKAN DIMAINKAN             \n");
	printf("___________________________________________________\n");
	printf("___________________________________________________\n");
	printf("===> Tekan 1 untuk memilih Quiz Game\n");
	printf("===> Tekan 2 untuk memilih Typing Game\n");
	printf("===> Tekan 3 untuk memilih 24 Game\n");
	printf("===> Tekan Q untuk kembali ke menu utama\n");
	printf("___________________________________________________\n");
	printf("___________________________________________________\n");
	char choice;
	for(int i=0; i<1; i++){
		choice=toupper(getch());
		switch(choice){
			case 'Q':
				mainMenu();
				break;
			case '1':
				main_Quiz();
				break;
			case '2':
				main_Typing();
				break;
			case '3':
				main_24();
				break;
			default:
				i--;
				break;		
		}
	}
}

void exitAnimation(){
	system("cls");
	printf("                                    )                \n");
	printf("  *   )                          ( /(             )  \n");
	printf("` )  /(  (  (  (     )      )    )\\())  )   (  ( /(  \n");
	printf(" ( )(_))))\\ )( )\\   (    ( /(  |((_)\\( /( ( )\\ )\\()) \n");
	printf("(_(_())/((_|()((_)  )\\  ')(_)) |_ ((_)(_)))((_|(_)\\  \n");
	printf("|_   _(_))  ((_|_)_((_))((_)_  | |/ ((_)_((_|_) |(_) \n");
	printf("  | | / -_)| '_| | '  \\() _` | | ' </ _` (_-< | ' \\  \n");
	printf("  |_| \\___||_| |_|_|_|_|\\__,_| |_|\\_\\__,_/__/_|_||_| \n");
	exit(1);
}
